FrontEnd do gestor de atividades
---

## Tecnologias Usadas:

1. yeoman (generator para estruturas de diretórios).
2. angularJS 1.4.0
3. bootstrap 3.2.0
4. lodash 4.17 (utilitario para comparações e manipulações de dados).
5. bower (gerenciador de dependencias).
6. gulp (automatizador de tarefas)
