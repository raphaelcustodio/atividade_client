'use strict';

describe('Controller: ListatividadeCtrl', function () {

  // load the controller's module
  beforeEach(module('atividadeClientApp'));

  var ListatividadeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ListatividadeCtrl = $controller('ListatividadeCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ListatividadeCtrl.awesomeThings.length).toBe(3);
  });
});
