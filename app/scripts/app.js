'use strict';

/**
 * @ngdoc overview
 * @name atividadeClientApp
 * @description
 * # atividadeClientApp
 *
 * Main module of the application.
 */
angular
  .module('atividadeClientApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider, $httpProvider) {
    $routeProvider
      .when('/listAtividade', {
        templateUrl: 'views/listAtividade.html',
        controller: 'atividadeController'
      })
      .when('/cadAtividade', {
        templateUrl: 'views/cadAtividade.html',
        controller: 'atividadeController',
		inserting: true,
		editing: false
      })
      .when('/editAtividade/:data', {
        templateUrl: 'views/cadAtividade.html',
        controller: 'atividadeController',
		editing: true,
		inserting: false
      })
      .otherwise({
        redirectTo: '/',
        templateUrl: 'views/home.html',
        controller: 'homeController'
      });

  	$httpProvider.defaults.headers.post["Content-Type"] = "application/json;charset=utf-8";
  	delete $httpProvider.defaults.headers.common['X-Requested-With'];
  	$httpProvider.defaults.useXDomain = true;

  });
