'use strict';
angular.module('atividadeClientApp').factory('atividadeFactory', ['$http', function($http) {

	var urlBase = 'http://localhost:8080/AvDevTrier/AtividadeRest/';
	var atividadeFactory = {};

	atividadeFactory.findAllAtividadeActive = function () {
    	return $http.get(urlBase + "findAllAtividadeActive");
	};

	atividadeFactory.findAll = function () {
		return $http.get(urlBase + "findAll");
	};

	atividadeFactory.findAllByExample = function (data) {
	    return $http.get(urlBase + "findAllByExample/" + btoa(JSON.stringify(data)));
	};

	atividadeFactory.findByPrimaryKey = function (id) {
	    return $http.get(urlBase + 'findByPrimaryKey/' + btoa(id));
	};

	atividadeFactory.insert = function (data) {
	    return $http.post(urlBase + "insert", data, getConfig("POST"));
	};

	atividadeFactory.update = function (data) {
	    return $http.post(urlBase + 'update', data, getConfig("POST"));
	};

	atividadeFactory.delete = function (data) {
    	return $http.post(urlBase + 'remove', data, getConfig("POST"));
	};

	atividadeFactory.finalizar = function (data) {
    	return $http.post(urlBase + 'finalizar', data, getConfig("POST"));
	};

	var getConfig = function(method) {
		return {
			method: method,
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		};
	}

	return atividadeFactory;
}]);
