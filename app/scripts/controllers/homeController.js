'use strict';

/**
 * @ngdoc function
 * @name atividadeClientApp.controller:homeController
 * @description
 * # homeController
 * Controller of the atividadeClientApp
 */
angular.module('atividadeClientApp')
  .controller('homeController', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
