'use strict';

/**
 * @ngdoc function
 * @name atividadeClientApp.controller:atividadeController
 * @description
 * # atividadeController
 * Controller of the atividadeController
 */
angular.module('atividadeClientApp').controller('atividadeController', function ($scope, $route, $routeParams, atividadeFactory, $timeout, $location) {

	const LODASH = _;
	const FL_SIM_NAO_SELECT_TODOS = {id: 0, name: 'Todos'};
	const FL_SIM_NAO_SELECT_SIM = {id: 1, name: 'Sim'};
	const FL_SIM_NAO_SELECT_NAO = {id: 2, name: 'Não'};

	const TP_ATIVIDADE_DESENVOLVIMENTO = {id: 1, name: 'Desenvolvimento'};
	const TP_ATIVIDADE_ATENDIMENTO = {id: 2, name: 'Atendimento'};
	const TP_ATIVIDADE_MANUTENCAO = {id: 3, name: 'Manutenção'};
	const TP_ATIVIDADE_MANUTENCAO_URGENTE = {id: 4, name: 'Manutenção Urgente'};

	$scope.editing = false;
	$scope.domainEdit = {};
	$scope.domainList = [];
	$scope.domainFilter = {};
	$scope.editedDomain = {};

	$scope.inserting = $route.current.$$route.inserting;
	$scope.editing = $route.current.$$route.editing;
	$scope.errorMessage = undefined;
	if ($routeParams && $routeParams.data) {
		$scope.cdAtividadeEdit = $routeParams.data;
	}


	if(!$scope.editing && !$scope.inserting) {
		$scope.flAtivoSelectValue = FL_SIM_NAO_SELECT_TODOS;
		$scope.flFinalizadaSelectValue = FL_SIM_NAO_SELECT_TODOS;
		$scope.tipoAtividadeFilter = FL_SIM_NAO_SELECT_TODOS;
		$scope.flSimNaoOptions = [
		  FL_SIM_NAO_SELECT_TODOS,
		  FL_SIM_NAO_SELECT_SIM,
		  FL_SIM_NAO_SELECT_NAO
		];
		$scope.tipoAtividadeOptions = [
			FL_SIM_NAO_SELECT_TODOS,
			TP_ATIVIDADE_DESENVOLVIMENTO,
			TP_ATIVIDADE_ATENDIMENTO,
			TP_ATIVIDADE_MANUTENCAO,
			TP_ATIVIDADE_MANUTENCAO_URGENTE
		];
	} else {
		$scope.flAtivoSelectValue = FL_SIM_NAO_SELECT_SIM;
		$scope.flFinalizadaSelectValue = FL_SIM_NAO_SELECT_NAO;
		$scope.tipoAtividadeFilter = TP_ATIVIDADE_DESENVOLVIMENTO;
		$scope.flSimNaoOptions = [
		  FL_SIM_NAO_SELECT_SIM,
		  FL_SIM_NAO_SELECT_NAO
		];
		$scope.tipoAtividadeOptions = [
			TP_ATIVIDADE_DESENVOLVIMENTO,
			TP_ATIVIDADE_ATENDIMENTO,
			TP_ATIVIDADE_MANUTENCAO,
			TP_ATIVIDADE_MANUTENCAO_URGENTE
		];
	}


	var getAtividadeEdit = function	() {
		atividadeFactory.findByPrimaryKey($scope.cdAtividadeEdit).then(function (response) {
			$scope.flAtivoSelectValue = (response.data.isAtivo) ? FL_SIM_NAO_SELECT_SIM : FL_SIM_NAO_SELECT_NAO;
			$scope.flFinalizadaSelectValue = (response.data.isFinalizada) ? FL_SIM_NAO_SELECT_SIM : FL_SIM_NAO_SELECT_NAO;
			$scope.tipoAtividadeFilter = $scope.tipoAtividadeOptions[response.data.cdTipoAtividade-1];
			$scope.domainEdit = response.data;
			$scope.errorMessage = undefined;
		}, function (error) {
			$scope.domainEdit = {};
			$scope.errorMessage = error;
			console.error("Erro ao obter a atividade, Detalhes: ", error);
		});
	};

	var setDynamicFlSimNaoFilter = function(selectValue, ignoreBooleanFilterValue, filterBooleanValue) {
		if (LODASH.isEqual($scope[selectValue], FL_SIM_NAO_SELECT_TODOS)) {
			$scope.domainFilter[ignoreBooleanFilterValue] = true;
		} else {
			$scope.domainFilter[ignoreBooleanFilterValue] = false;
			$scope.domainFilter[filterBooleanValue] = LODASH.isEqual($scope[selectValue], FL_SIM_NAO_SELECT_SIM) ? true : false;
		}
	};

	var getDomainFilter = function() {
		setDynamicFlSimNaoFilter("flAtivoSelectValue", "isIgnoreFlAtivoFilter", "isAtivo");
		setDynamicFlSimNaoFilter("flFinalizadaSelectValue", "isIgnoreFlFinalizadaFilter", "isFinalizada");
		if (!LODASH.isEqual($scope.tipoAtividadeFilter, FL_SIM_NAO_SELECT_TODOS)) {
			$scope.domainFilter.cdAtividade = $scope.tipoAtividadeFilter.id;
		} else {
			$scope.domainFilter.cdAtividade = null;
		}
		return $scope.domainFilter;
	};

	$scope.list = function() {
		atividadeFactory.findAllByExample(getDomainFilter()).then(function (response) {
            $scope.domainList = response.data;
			$scope.errorMessage = undefined;
        }, function (error) {
			$scope.errorMessage = error;
			$scope.domainList = [];
            console.error("Erro ao obter as atividades, Detalhes: ", error);
        });
	};

	$scope.removerAtividade = function (data) {
		atividadeFactory.delete(data).then(function (response) {
            data.isAtivo = !data.isAtivo;
			$scope.errorMessage = undefined;
        }, function (error) {
			$scope.errorMessage = error;
            console.error("Erro ao remover a atividade, Detalhes: ", error);
        });
	};

	$scope.finalizaAtividade = function (data) {
		atividadeFactory.finalizar(data).then(function (response) {
            data.isFinalizada = !data.isFinalizada;
			$scope.errorMessage = undefined;
        }, function (error) {
			$scope.errorMessage = error;
            console.error("Erro ao finalizar a atividade, Detalhes: ", error);
        });
	}

	$scope.toBase64Domain = function(jsonValue) {
		return btoa(JSON.stringify(jsonValue));
	};

	var prepareDataValues = function() {
		$scope.domainEdit.isAtivo = LODASH.isEqual($scope.flAtivoSelectValue, FL_SIM_NAO_SELECT_SIM) ? true : false;
		$scope.domainEdit.isFinalizada = LODASH.isEqual($scope.flFinalizadaSelectValue, FL_SIM_NAO_SELECT_SIM) ? true : false;
		$scope.domainEdit.cdTipoAtividade = $scope.tipoAtividadeFilter.id;
	}

	var afterInsertOrUpdate = function(response) {
		$location.path("/listAtividade");
	}

	$scope.insertOrUpdate = function() {
		prepareDataValues();
		if ($scope.inserting) {
			atividadeFactory.insert($scope.domainEdit).then(function (response) {
				afterInsertOrUpdate(response);
				$scope.errorMessage = undefined;
			}, function (error) {
				$scope.errorMessage = error;
				console.error("Erro ao inserir a atividade, Detalhes: ", error);
			});
		} else {
			atividadeFactory.update($scope.domainEdit).then(function (response) {
				afterInsertOrUpdate(response);
				$scope.errorMessage = undefined;
			}, function (error) {
				$scope.errorMessage = error;
				console.error("Erro ao atualizar a atividade, Detalhes: ", error);
			});
		}
	}

	if (!$scope.editing) {
		$scope.list();
	} else {
		getAtividadeEdit();
	}

});
